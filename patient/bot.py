import sys

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options as FFOptions
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from time import sleep

import config
from myLogger import logger


class SiteEvents:
    def __init__(self):
        logger.info('Preparing Browser...')

        firefox_options = FFOptions()
        firefox_options.add_argument('--headless')
        firefox_profile = webdriver.FirefoxProfile()
        firefox_profile.set_preference('permissions.default.image', 2)

        # firefox_binary = r'C:\geckodriver'
        # self.driver = webdriver.Firefox(firefox_options=firefox_options, firefox_profile=firefox_profile,
        #                                 executable_path=firefox_binary)
        self.driver = webdriver.Firefox(firefox_options=firefox_options, firefox_profile=firefox_profile)
        self.driver.implicitly_wait(30)
        self.driver.set_page_load_timeout(20)

    def teardown(self):
        try:
            self.driver.quit()
        except:
            pass

    def login(self, username, password):
        logger.info('Logging In to DrChrono EHR System...')
        url = 'https://app.drchrono.com/accounts/login/'
        self.driver.get(url)

        self.driver.find_element_by_name('username').send_keys(username)
        self.driver.find_element_by_name('password').send_keys(password)
        self.driver.find_element_by_id('login').click()

        try:
            WebDriverWait(self.driver, 20).until(
                EC.visibility_of_element_located((By.XPATH, '//a[@id="switch-button"]/i[@class="icon-user"]')))
            logger.info('Login Successful!')
            return True
        except TimeoutException:
            logger.info('Login FAILED!')
            return False

    def create_new_patient(self, patient_info_dict):
        logger.info('Creating New Patient...')
        url = 'https://advichrono.drchrono.com/patients/new/'
        self.driver.get(url)

        self.driver.find_element_by_name('first_name').send_keys(patient_info_dict['first_name'])
        self.driver.find_element_by_name('nick_name').send_keys(patient_info_dict['nick_name'])
        self.driver.find_element_by_name('middle_name').send_keys(patient_info_dict['middle_name'])
        self.driver.find_element_by_name('last_name').send_keys(patient_info_dict['last_name'])

        self.driver.execute_script(
            'document.getElementsByClassName("navbar navbar-fixed-bottom")[0].style.display = "None";')

        self.driver.find_element_by_xpath('//a[@class="nav_link" and .="Demographics"]').click()

        self.driver.execute_script('document.getElementById("id_social_security_number").value="{}";'.format(
            patient_info_dict['social_security_number']))
        self.driver.execute_script('document.getElementById("id_date_of_birth").value="{}";'.format(
            patient_info_dict['date_of_birth']))

        self.driver.find_element_by_name('age_approximate').send_keys(patient_info_dict['age_approximate'])
        Select(self.driver.find_element_by_name('gender')).select_by_visible_text(patient_info_dict['sex'])
        Select(self.driver.find_element_by_name('gender_identity')).select_by_visible_text(patient_info_dict['gender_identity'])
        Select(self.driver.find_element_by_name('sexual_orientation')).select_by_visible_text(patient_info_dict['sexual_orientation'])

        if len(patient_info_dict['race']) > 0:
            self.driver.find_element_by_xpath('//select[@id="id_multi_race"]/following-sibling::div/button').click()
            for race in patient_info_dict['race']:
                race_xpath = '//select[@id="id_multi_race"]/following-sibling::div/ul/li//label[contains(., "{}")]/input'.format(race)
                self.driver.find_element_by_xpath(race_xpath).click()
            self.driver.find_element_by_xpath('//select[@id="id_multi_race"]/following-sibling::div/button').click()

        Select(self.driver.find_element_by_name('ethnicity')).select_by_visible_text(patient_info_dict['ethnicity'])
        Select(self.driver.find_element_by_name('preferred_language')).select_by_visible_text(patient_info_dict['preferred_language'])
        Select(self.driver.find_element_by_name('patient_student_status')).select_by_visible_text(patient_info_dict['student_status'])
        Select(self.driver.find_element_by_name('country')).select_by_visible_text(patient_info_dict['country'])
        self.driver.find_element_by_name('address').send_keys(patient_info_dict['address'])
        self.driver.find_element_by_name('patient_zip_code').send_keys(patient_info_dict['zip_code'])
        self.driver.find_element_by_name('city').send_keys(patient_info_dict['city'])

        Select(self.driver.find_element_by_name('patient_state')).select_by_visible_text(patient_info_dict['state'])
        self.driver.find_element_by_name('patient_county_code').send_keys(patient_info_dict['county_code'])

        self.driver.find_element_by_name('_save').click()
        sleep(1)

        messages = []
        for elem in self.driver.find_elements_by_xpath('//div[@class="jGrowl-message"]'):
            if elem.text.strip() != '':
                messages.append(elem.text.strip())
        return '\n'.join(messages)


def create_patient_bot(patient_info_dict):
    browser = SiteEvents()
    browser.login(config.username, config.password)
    try:
        message = browser.create_new_patient(patient_info_dict)
        return True, message
    except Exception as e:
        browser.teardown()
        logger.exception(sys.exc_info())
        return False, str(e)


if __name__ == '__main__':
    patient = {
        'first_name': 'fname',
        'nick_name': 'nick_name',
        'middle_name': 'J.',
        'last_name': 'lname',
        'social_security_number': '123456789',
        'date_of_birth': '10/30/1980',
        'age_approximate': '39',
        'sex': 'Male',
        'gender_identity': 'Male',
        'sexual_orientation': 'Heterosexual (not lesbian, gay, or bisexual)',
        'race': ['American Indian or Alaska Native', 'Black or African American'],
        'ethnicity': 'Hispanic or Latino',
        'preferred_language': 'English',
        'student_status': 'Full-time student',
        'country': 'UNITED STATES',
        'address': 'St Address',
        'zip_code': '49411',
        'city': 'Holland',
        'state': 'Michigan',
        'county_code': 'MI'
    }
    create_patient_bot(patient)
