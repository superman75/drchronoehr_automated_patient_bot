from django.urls import path

from . import views

urlpatterns = [
    path('create-patient', views.create_patient, name='create_patient'),
]
