import json

from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt

from patient.bot import create_patient_bot


@csrf_exempt
@require_http_methods(["POST"])
def create_patient(request):
    body = json.loads(request.body.decode())

    patient_info = body['patient_info']

    mimetype = 'application/json'
    if patient_info.get('first_name', None) in [None, '']:
        data = {'success': False, 'message': 'first_name is mandatory'}
        return HttpResponse(json.dumps(data), mimetype, status=400)
    if patient_info.get('last_name', None) in [None, '']:
        data = {'success': False, 'message': 'last_name is mandatory'}
        return HttpResponse(json.dumps(data), mimetype, status=400)

    result, message = create_patient_bot(patient_info)
    data = {'success': result, 'message': message}

    return HttpResponse(json.dumps(data), mimetype)
